import React, { Fragment, useEffect } from "react";
import MyMessage from "../MyMessage/MyMessage";
import TheirMessage from "../TheirMessage/TheirMessage";
import DayDelimiter from "../DayDelimiter/DayDelimiter";
import "./MessageList.css";

const MessageList = ({ activeUser, messages, onDelete, onLike, onEdit }) => {
  let prevDate = "";

  useEffect(() => {
    const messageBox = document.querySelector(".chat-message-list");
    messageBox.scrollTop = messageBox.scrollHeight;
  }, [messages.length]);

  return (
    <div className="chat-message-list">
      {messages.map((message) => {
        let delimiter = null;
        const date = getMessageDate(new Date(message.createdAt));
        console.log(prevDate);
        console.log(date);
        if (!compareDates(date, prevDate)) {
          console.log("a");
          const delimiterDate = `${date.day}.${date.month}.${date.year}`;
          prevDate = date;
          delimiter = <DayDelimiter date={delimiterDate} />;
        }
        if (message.userId == activeUser) {
          return (
            <Fragment key={message.id}>
              {delimiter}
              <MyMessage
                message={message}
                onDelete={onDelete}
                onEdit={onEdit}
              />
            </Fragment>
          );
        } else {
          return (
            <Fragment key={message.id}>
              {delimiter}
              <TheirMessage message={message} onLike={onLike} />
            </Fragment>
          );
        }
      })}
    </div>
  );
};

function getMessageDate(date) {
  return {
    day: date.getDate(),
    month: date.getMonth() + 1,
    year: date.getFullYear(),
  };
}

function compareDates(date, prevDate) {
  if (
    date.day == prevDate.day &&
    date.month == prevDate.month &&
    date.year == prevDate.year
  )
    return true;
  return false;
}

export default MessageList;
