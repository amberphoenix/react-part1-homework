import React, { useState, useEffect } from "react";
import "./MessageInput.css";

const MessageInput = ({
  onSendButtonClicked,
  onEditButtonClicked,
  editMessage,
}) => {
  const [inputValue, setInputValue] = useState("");
  const [isEditMessage, setIsEditMessage] = useState(false);

  useEffect(() => {
    if (editMessage) {
      setIsEditMessage(true);
      setInputValue(editMessage.text);
    }
  }, [editMessage]);

  const onInputChange = (event) => {
    setInputValue(event.target.value);
  };

  const onButtonClick = () => {
    if (!inputValue.trim()) return;
    if (isEditMessage) onEditButtonClicked(inputValue);
    else onSendButtonClicked(inputValue);
    setInputValue("");
  };

  return (
    <div className="send-area">
      <input
        className="input"
        placeholder="Type your message here.."
        onChange={onInputChange}
        value={inputValue}
      />
      <button className="button" onClick={onButtonClick}>
        SEND
      </button>
    </div>
  );
};

export default MessageInput;
