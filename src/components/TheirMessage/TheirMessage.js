import React from "react";
import "./TheirMessage.css";

const TheirMessage = ({ message, onLike }) => {
  const dateOfMessage = `${new Date(message.createdAt).getDate()}.${
    new Date(message.createdAt).getMonth() + 1
  }.${new Date(message.createdAt).getFullYear()}`;

  const timeOfMessage = `${new Date(message.createdAt).getHours()}:${new Date(
    message.createdAt
  ).getMinutes()}`;

  return (
    <div className="their-message">
      <img className="avatar" src={message.avatar} alt="User's avatar." />
      <div className="info-wrapper">
        <div className="message-info">
          <p className="user-name margin-0">{message.user}</p>
          <p className="message-date margin-0">
            {timeOfMessage} | {dateOfMessage}
          </p>
        </div>
        <p className="message-text margin-0">{message.text}</p>
        <div className="message-actions">
          <i
            className="fas fa-heart action"
            onClick={() => onLike(message.id)}
          ></i>
          <div>{message.liked.length}</div>
        </div>
      </div>
    </div>
  );
};

export default TheirMessage;
