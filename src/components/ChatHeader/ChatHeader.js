import React from "react";
import "./ChatHeader.css";

const ChatHeader = ({
  title,
  participantsNumber,
  messagesNumber,
  timeOfLastMessage,
}) => {
  return (
    <div className="chat-header">
      <div className="chat-info">
        <div className="chat-name">{title}</div>
        <div className="chat-participants">
          {participantsNumber} participants
        </div>
        <div className="chat-messages">{messagesNumber} messages</div>
      </div>
      <div className="chat-last-message">
        last message at {timeOfLastMessage}
      </div>
    </div>
  );
};

export default ChatHeader;
