import React from "react";
import "./MyMessage.css";

const MyMessage = ({ message, onDelete, onEdit }) => {
  const dateOfMessage = `${new Date(message.createdAt).getDate()}.${
    new Date(message.createdAt).getMonth() + 1
  }.${new Date(message.createdAt).getFullYear()}`;

  const timeOfMessage = `${new Date(message.createdAt).getHours()}:${new Date(
    message.createdAt
  ).getMinutes()}`;

  return (
    <div className="my-message">
      <div className="message-info">
        <p className="username margin-0">Me</p>
        <p className="message-date margin-0">
          {timeOfMessage} | {dateOfMessage}
        </p>
      </div>
      <p className="message-text margin-0">{message.text}</p>
      <div className="message-actions">
        <i className="fas fa-pen action" onClick={() => onEdit(message.id)}></i>
        <i
          className="fas fa-trash action"
          onClick={() => onDelete(message.id)}
        ></i>
      </div>
    </div>
  );
};

export default MyMessage;
