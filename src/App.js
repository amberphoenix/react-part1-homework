import Layout from "./hoc/Layout/Layout";
import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Chat from "./containers/Chat";

function App() {
  const routes = (
    <BrowserRouter>
      <Switch>
        <Route path="/" component={Chat} />
      </Switch>
    </BrowserRouter>
  );
  return <Layout>{routes}</Layout>;
}

export default App;
