import React, { useState, useEffect } from "react";
import ChatHeader from "../components/ChatHeader/ChatHeader";
import MessageInput from "../components/MessageInput/MessageInput";
import MessageList from "../components/MessageList/MessageList";
import axios from "axios";
import { v1 as uuid } from "uuid";
import "./Chat.css";

const Chat = () => {
  const [isLoading, setLoading] = useState(true);
  const [chatMessages, setChatMessages] = useState([]);
  const [messAmount, setMessAmount] = useState(0);
  const [lastMessDate, setLastMessDate] = useState("00:00");
  const [users, setUsers] = useState(new Map());
  const [usersCount, setUsersCount] = useState(0);
  const [activeUserId, setActiveUserId] = useState("0");
  const [editMessage, setEditMessage] = useState(null);

  useEffect(() => {
    axios
      .get("https://edikdolynskyi.github.io/react_sources/messages.json")
      .then(({ data: messages }) => {
        sortMessagesByDate(messages);
        messages.forEach((m) => (m.liked = []));
        setChatMessages(messages);
        setLoading(false);

        const messagesAmount = messages.length;
        setMessAmount(messagesAmount);

        const lastMessage = messages[messagesAmount - 1].createdAt;
        const lastMessageDate = `${new Date(lastMessage).getHours()}:${new Date(
          lastMessage
        ).getMinutes()}`;
        setLastMessDate(lastMessageDate);

        const participants = createUsersMap(messages);
        setUsers(participants);
        const participantsCount = participants.size;
        setUsersCount(participantsCount);

        const currentUserId = getCurrentUserId(messages);
        setActiveUserId(currentUserId);
      })
      .catch((error) => console.log(error));
  }, []);

  const onSendMessage = (input) => {
    const message = {
      id: uuid(),
      userId: activeUserId,
      avatar: users.get(activeUserId).avatar,
      user: users.get(activeUserId).user,
      text: input,
      createdAt: new Date(),
      editedAt: "",
      liked: [],
    };

    setChatMessages([...chatMessages, message]);
  };

  const onDeleteMessage = (messageId) => {
    const data = chatMessages.filter((message) => message.id !== messageId);
    const newLength = data.length;
    setChatMessages(data);
    setMessAmount(newLength);
  };

  const onLikeMessage = (messageId) => {
    const currMessage = chatMessages.find(({ id }) => id === messageId);
    if (currMessage.liked.includes(activeUserId)) {
      currMessage.liked = currMessage.liked.filter((id) => id !== activeUserId);
    } else {
      currMessage.liked.push(activeUserId);
    }

    setChatMessages([...chatMessages]);
  };

  const onEditMessage = (messageId) => {
    const currMessage = chatMessages.find(({ id }) => id === messageId);
    setEditMessage(currMessage);
  };

  const onEdit = (input) => {
    const currMessage = chatMessages.find(({ id }) => id === editMessage.id);
    currMessage.text = input;
    currMessage.editedAt = new Date();

    setChatMessages([...chatMessages]);
    setEditMessage(null);
  };

  return isLoading ? (
    <p className="loader">let's wake up the Chatto..</p>
  ) : (
    <>
      <ChatHeader
        title={"MyChat"}
        participantsNumber={usersCount}
        messagesNumber={messAmount}
        timeOfLastMessage={lastMessDate}
      />
      <MessageList
        activeUser={activeUserId}
        messages={chatMessages}
        onDelete={onDeleteMessage}
        onLike={onLikeMessage}
        onEdit={onEditMessage}
      />
      <MessageInput
        onSendButtonClicked={onSendMessage}
        onEditButtonClicked={onEdit}
        editMessage={editMessage}
      />
    </>
  );
};

function sortMessagesByDate(messages) {
  messages.sort((a, b) => {
    return new Date(a.createdAt) - new Date(b.createdAt);
  });
}

function createUsersMap(messages) {
  const users = new Map();
  messages.forEach((message) => {
    if (!users.has(message.userId)) {
      users.set(message.userId, {
        username: message.user,
        avatar: message.avatar,
      });
    }
  });
  return users;
}

function getCurrentUserId(messages) {
  const numberOfMessage = Math.floor(Math.random() * messages.length);
  return messages[numberOfMessage].userId;
}

export default Chat;
